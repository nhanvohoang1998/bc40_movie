import React, { Fragment } from 'react'
import { useSelector } from 'react-redux'
import { NavLink } from 'react-router-dom'
import { localUserServ } from '../../service/localService'
import UserDropdown from './UserDropdownn'

export default function UserMenu() {
  let userInfor = useSelector((state) => {
    return state.userReducer.userInfor
  })
  console.log("user info", userInfor)
  let handleLogout = () => {
    localUserServ.remove();
    window.location.reload();
    // window.location.href="./login"
  }
  let renderContent = () => {
    let buttonCss = "px-5 py-2 border-2 border-black"
    if (userInfor) {
      //đăng nhập
      return (
        <Fragment>
          <UserDropdown user={userInfor} logoutBtn={<button className={buttonCss} onClick={handleLogout}>Đăng xuất</button>}/>
          {/* <button className={buttonCss} onClick={handleLogout}>Đăng xuất</button> */}
        </Fragment>
      )
    } else {
      return (
        <Fragment>
          <NavLink to="/login">
            <button className={buttonCss}>Đăng nhập</button>
          </NavLink>
          <button className={buttonCss}>Đăng ký</button>
        </Fragment>
      )
    }
  }

  return (
    <div className='space-x-5'>
      {renderContent()}
    </div>
  )
}
