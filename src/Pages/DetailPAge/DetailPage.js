import React from 'react'
import { NavLink, useParams } from 'react-router-dom';
import { movieSer } from '../service/movieService';
import { useEffect, useState } from 'react'
import { Progress } from 'antd';

export default function DetailPage() {
  let { id } = useParams();
  console.log(id);
  const [movie, setMovie] = useState([])

  useEffect(() => {
    let fetchDetail = async () => {
      try {
        let result = await movieSer.getDetailMovie(id)
        setMovie(result.data.content)
        console.log("🚀 ~ file: DetailPage.js:12 ~ fetchDetail ~ result:", result)

      } catch (error) {
        console.log("🚀 ~ file: DetailPage.js:15 ~ fetchDetail ~ error:", error)

      }
    }
    fetchDetail();
  }, [])
  //async await ~ the catch


  return (
    <div className='container'>
      <div className='flex space-x-10'>
        <img src={movie.hinhAnh} className='w-1/3' alt="" />
        <div>
          <h2 className='font-medium'>{movie.tenPhim}</h2>
          <h2>{movie.moTa}</h2>
          <Progress percent={movie.danhGia*10}/>
        </div>
      </div>
      <NavLink className='rounded px-5 py-2 bg-red-600 text-white font-medium ' to={`/booking/${id}`}>
        Mua vé
      </NavLink>
    </div>
  )
}
