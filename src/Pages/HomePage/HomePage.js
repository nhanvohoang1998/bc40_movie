import Header from "../Components/Header/Header"
import React from 'react'
import ListMovie from "./ListMovie/ListMovie"
import TabsMovie from "./TabsMovie/TabsMovie"

export default function HomePage() {
  return (
    <div className="space-y-10">
      <Header/>
      <ListMovie/>
      <TabsMovie/>
      <br /><br /><br /><br />
    </div>
  )
}
