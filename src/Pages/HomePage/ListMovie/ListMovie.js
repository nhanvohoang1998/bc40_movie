import React, { useEffect, useState } from 'react'
import { movieSer } from '../../service/movieService'
import ItemMovie from './ItemMovie';

export default function ListMovie() {
    const [movies, setMovies] = useState([])
    useEffect(()=>{
        movieSer
            .getMovieList()
            .then((res) => {
                    console.log(res.data.content);
                    setMovies(res.data.content)
                  })
                  .catch((err) => {
                   console.log(err);
                  });
    }, [])

    return (
        <div className='container grid grid-cols-6 gap-10'>
            {movies.map((item)=>{
                return <ItemMovie data={item} key={item.maPhim}/>
            })}
        </div>
    )
}
