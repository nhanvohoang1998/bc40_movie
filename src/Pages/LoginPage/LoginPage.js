import { Button, Checkbox, Form, Input, message } from 'antd';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { USER_LOGIN } from '../redux/contant/userContant';
import { localUserServ } from '../service/localService';
import { userServ } from '../service/userService';
import Lottie from "lottie-react";
import bg_animate from '../../asset/login_animate.json'
import { setLoginAction, setLoginActionService } from '../redux/action/userAction';

const LoginPage = () => {
  let dispatch = useDispatch();
  let navigate = useNavigate();
  const onFinish = (values) => {
    console.log('Success:', values);
    userServ
      .postLogin(values)
      .then((res) => {
        message.success("login thành công")
        //lưu thông tin user vào localStorage
        localUserServ.set(res.data.content)
        dispatch(setLoginAction(res.data.content))
        //chuyển hướng user đến home
        navigate("/")
        console.log(res);
      })
      .catch((err) => {
        message.error("login thất bại")
        console.log(err);
      });
  };

  const onFinishThunk = (values) => {
    //callback
    let onSuccess = () => {
      message.success("login thành công")

      //chuyển hướng user đến home
      navigate("/")
    }
    dispatch(setLoginActionService(values, onSuccess))
  }

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };
  return (
    <div className="h-screen w-screen flex bg-orange-500 justify-center items-center">
      <div className="container mx-auto p-5 bg-white rounded flex">
        <div className='w-1/2 h-full'>
          <Lottie animationData={bg_animate}></Lottie>
        </div>
        <div className='w-1/2 h-full'>
          <Form
            name="basic"
            labelCol={{
              span: 8,
            }}
            wrapperCol={{
              span: 24,
            }}
            style={{
              maxWidth: 600,
            }}
            initialValues={{
              remember: true,
            }}
            onFinish={onFinishThunk}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
          >
            <Form.Item
              label="Username"
              name="taiKhoan"
              rules={[
                {
                  required: true,
                  message: 'Please input your username!',
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label="Password"
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: 'Please input your password!',
                },
              ]}
            >
              <Input.Password />
            </Form.Item>
            <Form.Item
              wrapperCol={{
                span: 24,
              }}
              className="flex justify-center items-center"
            >
              <Button className='bg-orange-500 hover:text-white hover:border-hidden' type="primary" htmlType="submit">
                Submit
              </Button>
            </Form.Item>
          </Form>
        </div>

      </div>
    </div>
  );
}
export default LoginPage;