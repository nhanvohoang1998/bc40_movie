import { message } from "antd"
import { localUserServ } from "../../service/localService"
import { userServ } from "../../service/userService"
import { USER_LOGIN } from "../contant/userContant"


export const setLoginAction = (value) => {
    //value đến từ response axios
    return {
        type: USER_LOGIN,
        payload: value,
    }
}

export const setLoginActionService = (value, onCompleted) => {
    //value đến thẻ form của antd
    return (dispatch) => {
        userServ
            .postLogin(value)
            .then((res) => {
                console.log(res);
                dispatch({
                    type: USER_LOGIN,
                    payload: res.data.content,
                })
                //lưu thông tin user vào localStorage
                localUserServ.set(res.data.content)
                onCompleted()
            })
            .catch((err) => {
                console.log(err);
            });
    }
}

//reduxthunk gọi api trong action