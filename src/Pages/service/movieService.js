import axios from "axios"
import { BASE_URL, configHeaders, https } from "./config"

export const movieSer = {
    getMovieList: () => {
        // return axios({
        //     url: `${BASE_URL}/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP01`,
        //     method: "GET",
        //     headers: configHeaders(),
        // })
        return https.get(`/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP01`)
    },
    getMovieByTheater: () => {
        // return axios({
        //     url: `${BASE_URL}/api/QuanLyRap/LayThongTinLichChieuHeThongRap`,
        //     method: "GET",
        //     headers: configHeaders(),
        // })
        return https.get(`${BASE_URL}/api/QuanLyRap/LayThongTinLichChieuHeThongRap`)
    },
    getDetailMovie: (maPhim)=>{
        return https.get(`api/QuanLyPhim/LayThongTinPhim?MaPhim=${maPhim}`)
    }
}